import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export interface Reservation {
  dateFrom: NgbDateStruct | any;
  dateTo: NgbDateStruct | any;
  room: number;
  rooms?: Rooms[];
}

export interface Rooms {
  adult: number;
  child: number;
}
