import { MealplanPipe } from './mealplan.pipe';

describe('MealplanPipe', () => {
  let pipe: MealplanPipe;

  beforeEach(() => {
    pipe = new MealplanPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty', () => {
    expect(pipe.transform(null)).toBe('');
  });

  it('should return empty', () => {
    expect(pipe.transform('')).toBe('');
  });

  it('should return wifi', () => {
    expect(pipe.transform('BREAKFAST')).toBe('Breakfast');
  });

  it('should return glass', () => {
    expect(pipe.transform('HALF_BOARD')).toBe('Half Board');
  });
});
