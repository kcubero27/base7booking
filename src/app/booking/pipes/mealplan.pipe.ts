import { Pipe, PipeTransform } from '@angular/core';

/**
 * Transforms a mealplan type to a string
 */
@Pipe({
  name: 'mealplan',
})
export class MealplanPipe implements PipeTransform {

  /**
   * Returns a mealplan name from a mealplan type
   * @param value mealplan type
   */
  transform(value: string): string {
    if (value) {
      switch (value) {
        case 'BREAKFAST':
          return 'Breakfast';
        case 'ALL_INCLUDE':
          return 'All Include';
        case 'HALF_BOARD':
          return 'Half Board';
        default:
          return '';
      }
    }
    return '';
  }
}
