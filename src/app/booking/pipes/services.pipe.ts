import { Pipe, PipeTransform } from '@angular/core';

/**
 * Transforms a service name to a class icon
 */
@Pipe({
  name: 'services',
})
export class ServicesPipe implements PipeTransform {
  /**
   * Returns an icon from a service
   * @param value service
   */
  transform(value: string): string {
    if (value) {
      switch (value) {
        case 'WIFI':
          return 'wifi';
        case 'PHONE':
          return 'phone';
        case 'BATH':
          return 'bath';
        case 'PARKING':
          return 'car';
        case 'BAR':
          return 'glass';
        case 'AIR':
          return 'snowflake-o';
        default:
          break;
      }
    }
    return '';
  }
}
