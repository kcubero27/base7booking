import { Component, OnInit, OnDestroy } from '@angular/core';
import { WizardService } from '../../../../shared/services/wizard.service';
import { Room } from '../../../../shared/interfaces/room.interface';
import { Apollo } from 'apollo-angular';
import { allRooms } from '../../../graphql/query/allRooms';
import { Subscription } from 'rxjs/Subscription';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { ReservationService } from '../../../../core/services/reservation.service';
import { FormGroup, FormArray } from '@angular/forms';
import { RoomService } from '../../../../core/services/room.service';
import { SelectedRoom } from '../../../interfaces/selected-room.interface';

/**
 * Component to select rooms
 */
@Component({
  selector: 'b7b-choose-room',
  templateUrl: './choose-room.component.html',
  styleUrls: ['./choose-room.component.scss'],
})
export class ChooseRoomComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[];
  reservation: FormGroup;
  selectedRooms: SelectedRoom[];
  rooms: Room[];

  constructor(
    private wizardService: WizardService,
    private apollo: Apollo,
    private spinnerService: SpinnerService,
    private reservationService: ReservationService,
    private roomService: RoomService
  ) {}

  /**
   * Init variables and executes an availability
   */
  ngOnInit() {
    this.subscriptions = [];

    this.initVariables();
    this.subscriptions.push(
      this.reservationService.reservation$.subscribe(
        res => {
          this.reservation = this.reservationService.transformToForm(res);
        },
        // TODO track errors for stadistics
        err => console.log(err)
      )
    );
    this.subscriptions.push(
      this.roomService.room$.subscribe(
        res => (this.selectedRooms = res),
        // TODO track errors for stadistics
        err => console.log(err)
      )
    );
    this.getAvailability();
  }

  /**
   * Init variables
   */
  private initVariables() {
    this.selectedRooms = [];
    this.rooms = [];
  }

  /**
   * Gets availability
   */
  private getAvailability() {
    this.spinnerService.start();
    this.subscriptions[2] = this.apollo
      .watchQuery<any>({
        query: allRooms,
      })
      .subscribe(
        ({ data }) => {
          this.rooms = data.allRooms;
          this.spinnerService.end();
        },
        // TODO track errors for stadistics
        err => console.log(err)
      );
  }

  /**
   * Add a new room to the selected ones
   * @param search form
   */
  onAdd($event) {
    if ($event.action === 'ADD_ROOM') {
      const control = (this.reservation.get('rooms') as FormArray).controls[
        this.selectedRooms.length
      ];
      const selectedRoom: SelectedRoom = {
        adult: parseInt(control.get('adult').value),
        child: parseInt(control.get('child').value),
        config: {
          type: $event.data.room.type,
          mealplan: $event.data.board.type,
          totalPrice: $event.data.board.price + $event.data.room.price,
        },
      };
      this.selectedRooms.push(selectedRoom);
      this.roomService.next(this.selectedRooms);
    }
  }

  /**
   * Removes a room from the selected ones
   * @param event from child component
   */
  onRemove($event) {
    if ($event.action === 'REMOVE_ROOM') {
      this.selectedRooms.splice(-1, 1);
      this.roomService.next(this.selectedRooms);
    }
  }

  /**
   * Goes to the next step in the wizard
   * @param event from child component
   */
  goNextStep($event) {
    if ($event.action === 'GO_NEXT_STEP') {
      this.wizardService.onNext();
    }
  }

  /**
   * Unsubscribe from all subscriptions
   */
  ngOnDestroy() {
    this.subscriptions.map(subscription => subscription.unsubscribe());
  }
}
