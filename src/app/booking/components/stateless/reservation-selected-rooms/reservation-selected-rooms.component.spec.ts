import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationSelectedRoomsComponent } from './reservation-selected-rooms.component';

describe('ReservationSelectedRoomsComponent', () => {
  let component: ReservationSelectedRoomsComponent;
  let fixture: ComponentFixture<ReservationSelectedRoomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationSelectedRoomsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationSelectedRoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  // TODO spec

  // it('should be created', () => {
  //   expect(component).toBeTruthy();
  // });
});
