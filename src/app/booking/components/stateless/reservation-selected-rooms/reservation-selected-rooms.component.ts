import { Component, Input } from '@angular/core';
import { SelectedRoom } from '../../../interfaces/selected-room.interface';

/**
 * Shows information about one single room already selected
 */
@Component({
  selector: 'b7b-reservation-selected-rooms',
  templateUrl: './reservation-selected-rooms.component.html',
  styleUrls: ['./reservation-selected-rooms.component.scss'],
})
export class ReservationSelectedRoomsComponent {
  @Input() selectedRooms: SelectedRoom[] = [];

  constructor() {}
}
