import { Component, EventEmitter, Output } from '@angular/core';

/**
 * Displays a message when the user has selected all the rooms
 */
@Component({
  selector: 'b7b-reservation-already-selected',
  templateUrl: './reservation-already-selected.component.html',
  styleUrls: ['./reservation-already-selected.component.scss'],
})
export class ReservationAlreadySelectedComponent {
  @Output() action: EventEmitter<any> = new EventEmitter();

  constructor() {}

  /**
   * Notifies to the parent component to pass to the next step
   */
  goNextStep() {
    this.action.emit({ action: 'GO_NEXT_STEP' });
  }
}
