import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationAlreadySelectedComponent } from './reservation-already-selected.component';

describe('ReservationAlreadySelectedComponent', () => {
  let component: ReservationAlreadySelectedComponent;
  let fixture: ComponentFixture<ReservationAlreadySelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationAlreadySelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationAlreadySelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
// TODO spec
  // it('should be created', () => {
  //   expect(component).toBeTruthy();
  // });
});
