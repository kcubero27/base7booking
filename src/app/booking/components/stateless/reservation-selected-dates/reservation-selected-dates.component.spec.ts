import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationSelectedDatesComponent } from './reservation-selected-dates.component';

describe('ReservationSelectedDatesComponent', () => {
  let component: ReservationSelectedDatesComponent;
  let fixture: ComponentFixture<ReservationSelectedDatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationSelectedDatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationSelectedDatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  // TODO spec

  // it('should be created', () => {
  //   expect(component).toBeTruthy();
  // });
});
