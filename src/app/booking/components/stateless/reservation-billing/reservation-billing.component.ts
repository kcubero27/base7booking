import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';

/**
 * Collects user data with a form
 */
@Component({
  selector: 'b7b-reservation-billing',
  templateUrl: './reservation-billing.component.html',
  styleUrls: ['./reservation-billing.component.scss'],
})
export class ReservationBillingComponent implements OnInit {
  @Output() action: EventEmitter<{action: string, data: {person: FormGroup}}> = new EventEmitter();
  billing: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  /**
   * Inits the reactive form
   */
  ngOnInit() {
    this.billing = this.fb.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      address: new FormGroup({
        street: new FormControl('', Validators.required),
        city: new FormControl('', Validators.required),
        country: new FormControl('', Validators.required)
      }),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('[^ @]*@[^ @]*'),
      ]),
      phone: new FormControl('', Validators.required),
      notes: new FormControl(''),
      payment: new FormControl('', Validators.required),
    });
  }

  /**
   * Emits the user information to the parent component
   */
  onBuy() {
    this.action.emit({action: 'BILLING', data: {person: this.billing}});
  }
}
