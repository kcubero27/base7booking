import { Component } from '@angular/core';

/**
 * Prints a static calendar to visualize days with availability or not
 */
@Component({
  selector: 'b7b-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent {
  constructor() {}
}
