import { Component, Input } from '@angular/core';
import { Person } from '../../../interfaces/person.interface';

/**
 * Last step in the booking flow
 */
@Component({
  selector: 'b7b-reservation-booking',
  templateUrl: './reservation-booking.component.html',
  styleUrls: ['./reservation-booking.component.scss'],
})
export class ReservationBookingComponent {
  @Input() person: Person;
  @Input() loc: string;

  constructor() {}
}
