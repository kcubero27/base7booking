import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SelectedRoom } from '../../../interfaces/selected-room.interface';

/**
 * Shows which room is going to be selected and which ones have already been selected
 */
@Component({
  selector: 'b7b-reservation-room',
  templateUrl: './reservation-room.component.html',
  styleUrls: ['./reservation-room.component.scss'],
})
export class ReservationRoomComponent {
  @Output() action = new EventEmitter<{ action: string }>();
  @Input() reservation: FormGroup;
  @Input() selectedRooms: SelectedRoom[] = [];

  constructor() {}

  /**
   * Notifies to the parent that the last selected room is going to be removed
   */
  onRemove() {
    this.action.emit({ action: 'REMOVE_ROOM' });
  }
}
