import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnDestroy,
} from '@angular/core';
import { WizardService } from '../../../services/wizard.service';
import { Step } from '../../../interfaces/step.interface';
import { Subscription } from 'rxjs/Subscription';

/**
 * Wizard component which prints steps
 */
@Component({
  selector: 'b7b-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardComponent implements OnInit, OnDestroy {
  @Output() finished: EventEmitter<boolean> = new EventEmitter();
  @Input() steps: Step[] = [];
  private subscription: Subscription;
  size: number;
  current: number;

  /**
   * Constructor
   * @param wizardService WizardService
   */
  constructor(private wizardService: WizardService) {
    this.subscription = this.wizardService.current$.subscribe(
      res => (this.current = res),
      // TODO track errors for stadistics
      err => console.log(err)
    );
  }

  /**
   * Init size
   */
  ngOnInit() {
    this.size = this.steps.length - 1;
  }

  /**
   * Next step
   */
  onNext(): void {
    this.wizardService.onNext();
  }

  /**
   * Previous step
   */
  onPrevious(): void {
    this.wizardService.onPrevious();
  }

  /**
   * Set current step
   * @param n step number
   */
  setCurrent(n: number): void {
    this.wizardService.setCurrent(n);
  }

  /**
   * Last step
   */
  onFinish(): void {
    this.finished.emit(true);
  }

  /**
   * Unsubscribee
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
