import { Component, OnInit, OnDestroy } from '@angular/core';
import { SpinnerService } from '../../../services/spinner.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Spinner component
 */
@Component({
  selector: 'b7b-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
})
export class SpinnerComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  loading: boolean;

  constructor(private spinnerService: SpinnerService) {
    this.subscription = this.spinnerService.spinner$.subscribe(
      res => {
        this.loading = res;
      },
      // TODO track errors for stadistics
      err => console.log(err)
    );
  }

  /**
   * Init loading to false
   */
  ngOnInit() {
    this.loading = false;
  }

  /**
   * Unsubscribe from the observable
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
