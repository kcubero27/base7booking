import { Hotel } from './hotel.interface';
import { Board } from './board.interface';

export interface Room {
  id: string;
  description: string;
  hotel: Hotel;
  type: string;
  services: any[];
  price: number;
  board: Board[];
  images: string[];
}
