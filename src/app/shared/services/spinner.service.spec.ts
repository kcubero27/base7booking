import { TestBed, inject } from '@angular/core/testing';

import { SpinnerService } from './spinner.service';

describe('SpinnerService', () => {
  let service: SpinnerService;
  let value: boolean;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpinnerService],
    });
    service = TestBed.get(SpinnerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be false', () => {
    service.spinner$.subscribe(res => (value = res));
    expect(value).toBeFalsy();
  });

  it('should be true', () => {
    service.spinner$.subscribe(res => (value = res));
    service.start();
    expect(value).toBeTruthy();
  });

  it('should be false', () => {
    service.spinner$.subscribe(res => (value = res));
    service.start();
    service.end();
    expect(value).toBeFalsy();
  });
});
