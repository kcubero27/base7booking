import { RoomService } from './room.service';
import { TestBed } from '@angular/core/testing';
import { SelectedRoom } from '../../booking/interfaces/selected-room.interface';

const rooms: SelectedRoom[] = [
  {
    adult: 1,
    child: 0,
    config: {
      type: '',
      mealplan: '',
      totalPrice: 100,
    },
  },
  {
    adult: 2,
    child: 1,
    config: {
      type: '',
      mealplan: '',
      totalPrice: 99,
    },
  },
];

describe('RoomService', () => {
  let service: RoomService;
  let value: SelectedRoom[] = [];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RoomService],
    });
    service = TestBed.get(RoomService);
    service.room$.subscribe(res => (value = res));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be empty', () => {
    expect(value.length).toBe(0);
  });

  it('should be 2', () => {
    service.next(rooms);
    expect(value.length).toBe(2);
  });
});
