import { Injectable } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray,
} from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {
  Reservation,
  Rooms,
} from '../../booking/interfaces/reservation.interface';

/**
 * Service which keeps data from the search form
 */
@Injectable()
export class ReservationService {
  config: Reservation = {
    dateFrom: {},
    dateTo: {},
    room: 1,
    rooms: [
      {
        adult: 1,
        child: 0,
      },
    ],
  };

  private reservation = new BehaviorSubject<Reservation>(this.config);
  reservation$ = this.reservation.asObservable();

  constructor(private fb: FormBuilder) {}

  /**
   * Pushes a new value of the form
   * @param form FormGroup of the search form
   */
  next(reservation: Reservation) {
    this.reservation.next(reservation);
  }

  /**
   * Returns a form from a Reservation object
   * @param reservation object to transform
   */
  transformToForm(reservation: Reservation): FormGroup {
    const form = this.fb.group({
      dateFrom: new FormControl(reservation.dateFrom, Validators.required),
      dateTo: new FormControl(reservation.dateTo, Validators.required),
      room: new FormControl(reservation.room, Validators.required),
      rooms: this.fb.array([]),
    });

    for (let i = 0; i < reservation.room; i++) {
      (form.get('rooms') as FormArray).push(this.addRoom(reservation.rooms[i]));
    }

    return form;
  }

  /**
   * Returns a form group from an object
   * @param room current Rooms
   */
  addRoom(room: Rooms) {
    return this.fb.group({
      adult: room.adult,
      child: room.child,
    });
  }

  /**
   * Returns a form transformed from a reservation object
   * @param form reservation FormGroup
   */
  transformToOject(form: FormGroup): Reservation {
    const reservation: Reservation = {
      dateFrom: form.get('dateFrom').value,
      dateTo: form.get('dateTo').value,
      room: parseInt(form.get('room').value),
      rooms: [],
    };

    (form.get('rooms') as FormArray).controls.map(x => {
      reservation.rooms.push({
        adult: parseInt(x.get('adult').value),
        child: parseInt(x.get('child').value),
      });
    });

    return reservation;
  }
}
