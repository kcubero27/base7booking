import { Injectable } from '@angular/core';
import { Http, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { environment } from '../../../environments/environment';
import { Person } from '../../booking/interfaces/person.interface';

/**
 * Handles all email functions
 */
@Injectable()
export class EmailService {
  constructor(private httpService: Http) {}

  /**
   * Sends an email to the client through a zappier hook
   * This hook is executed on a POST request
   * @param info client information
   */
  sendEmail(info) {
    this.httpService.post(environment.zappier, JSON.stringify(info)).subscribe(
      res => console.log('Email sent', res),
      // TODO handle error
      err => console.log('Error sending email', err)
    );
  }

  /**
   * Returns an specific object for sending the confirmation email
   * @param person information
   * @param bookingReference booking reference number
   */
  transformData(person: Person, bookingReference: string) {
    return {
      name: person.firstName,
      lastName: person.lastName,
      email: person.email,
      bookingReference: bookingReference,
    };
  }
}
