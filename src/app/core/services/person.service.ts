import { Injectable } from '@angular/core';
import { Person } from '../../booking/interfaces/person.interface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

/**
 * Handles information from a user
 */
@Injectable()
export class PersonService {
  private person = new BehaviorSubject<Person>(null);
  person$ = this.person.asObservable();

  constructor() {}

  /**
   * Adds a new value to the Observable
   * @param person object
   */
  next(person: Person) {
    this.person.next(person);
  }
}
