import { Injectable } from '@angular/core';
import { SelectedRoom } from '../../booking/interfaces/selected-room.interface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

/**
 * Handles selected rooms
 */
@Injectable()
export class RoomService {
  private room = new BehaviorSubject<SelectedRoom[]>([]);
  room$ = this.room.asObservable();

  constructor() {}

  /**
   * Adds a new value
   * @param room array of selected rooms
   */
  next(room: SelectedRoom[]) {
    this.room.next(room);
  }
}
