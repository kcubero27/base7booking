import { ApolloClient, createNetworkInterface } from 'apollo-client';
import { environment } from '../../../environments/environment';

// Create a simple network without real time subscriptions
const networkInterface = createNetworkInterface({ uri: environment.simple });

const client = new ApolloClient({ networkInterface });

/**
 * Returns an ApolloClient
 */
export function provideClient(): ApolloClient {
  return client;
}
