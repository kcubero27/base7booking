import { Routes, RouterModule } from '@angular/router';
import { BookingComponent } from './booking/components/statefull/booking/booking.component';
import { NotFoundComponent } from './booking/components/stateless/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: BookingComponent,
  },
  {
    path: '404',
    component: NotFoundComponent,
  },
  {
    path: '**',
    redirectTo: '404',
  },
];

export const AppRoutingModule = RouterModule.forRoot(routes);
