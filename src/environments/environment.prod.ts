/**
 * Prod and dev environments are the same
 */
export const environment = {
  production: true,
  simple: 'https://api.graph.cool/simple/v1/cj7qdrzi40l1y0149bs2gp38j',
  subscription: 'wss://subscriptions.graph.cool/v1/cj7qdrzi40l1y0149bs2gp38j',
  zappier: 'https://hooks.zapier.com/hooks/catch/2322616/iujx1h/',
};
